fun main(args: Array<String>) {

    val name = "ZCarlos"

    //Explicitar o tipo de variável do tipo string (sendo desnecessário)
    val nameString : String = "Zcarlos2"

    var isAwesome = false
    isAwesome = true

    println("Is " + name + " awesome? The answer is : " + isAwesome)

    var a = 3
    var b = 6
    println(a + b)

    //Explicitar o tipo de variável do tipo Int (sendo desnecessário)
    val aInt : Int = 3

    val doubleNum = 123.45  //Número de 64 bit (ou seja, suporta números muito maiores)
    val floatNum = 123.45f  //Número de 32 bit

    //Explicitar o tipo de variável do tipo double (sendo desnecessário)
    val doubleNumExplicitly : Double = 123.45

    val longNum = 12345678912845646L // Número de 64 bit (muito pouco usado)

    //Efetuar conversões entre formatos
    val aDouble = a.toDouble()  //Conversão de um int para double
    val aString = a.toString()  //Conver~sao de um int para string

    //Declarar uma variável sem a inicializar
    var hero : String
    hero = "Batman"
    hero = "Superman"
    println(hero)


}