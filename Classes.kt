////Uma class que possui duas propriedades
//class Car(val make: String, val model: String, var color: String) {
//    //Método
//    fun acelerate() {
//        println("vroom vroom")
//    }
//
//    fun details(){
//        println("This is a $color $make $model")
//    }
//}
//
//class Truck (val make: String, val model: String, val towingCapacity: Int) {
//    fun town() {
//        println("taking the horses to the rodeo")
//    }
//
//    fun details() {
//        println("The $make $model has a towing capacity of $towingCapacity lbs")
//    }
//}

// As Classes en Kotlin por defeito são finais, ou seja, não podem ser herdadas
// para que isso seja possível coloca-se 'open'
open class Vehicle(make: String, model: String) {

    val make = make
    val model = model

    open fun accelerate() {
        println("vroom vroom")
    }

    fun park() {
        println("parking the vehicle")
    }

    fun brake() {
        println("STOP")
    }
}

// Class Car que herda as propriedades da class Vehicle
// Se uma classe não possuir métodos não é necessário as chavetas
class Car(make: String, model: String, var color: String) : Vehicle(make, model) {
    // Efetuado alterações ao método accelerate
    override fun accelerate() {
        println("We are going ludicrous mode!")
    }
}
// Class Truck que herda as propriedades da class Vehicle
class Truck(make: String, model: String, val towingCapacity: Int) : Vehicle(make, model) {
    fun tow() {
        println("headed out to the mountains!")
    }
}

fun main(args: Array<String>) {

    val tesla = Car("Tesla", "ModelS", "Red")
    tesla.accelerate()

    val truck = Truck("Ford", "F-150", 10000)
    truck.accelerate()


//    val car = Car("Toyota", "Avalon", "Red")
//    println(car.make)
//    println(car.model)
//    car.acelerate()
//    car.details()
//
//    val truck = Truck("Ford", "F-150", 10000)
//    truck.town()
//    truck.details()



}