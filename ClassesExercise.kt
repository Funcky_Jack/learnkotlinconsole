open class Player(name: String, age: String, gender:String) {

    val name = name
    val age = age
    val gender = gender

    open fun attack(){
        println("Attacking")
    }

    fun defend() {
        println("Defending")
    }

    open fun walk() {
        println("Walking")
    }
}

class Thief(name: String, age: String, gender:String, take: Int = 1) : Player(name, age, gender) {

    var take = take
    override fun walk() {
        println("Is always running from the sheriff")
    }
}

class Warrior(name: String, age: String, gender:String, numWeapon: Int = 2) : Player(name, age, gender) {

    var numWeapon = numWeapon
    override fun attack() {
        println("Running to attack the enemy...")
    }
}

fun main(args: Array<String>) {
    val thief = Thief("Oscar", "23", "male")
    thief.take = 3
    println("The thief name is ${thief.name} and he is ${thief.age} years old is a ${thief.gender}, and is taken ${thief.take} item")
    thief.walk()

    val warrior = Warrior("Sir Devros", "22", "male")
    println("The great warrior name is ${warrior.name}, and he is ${warrior.age} years old, to defend himself he as ${warrior.numWeapon} weapons")
    warrior.attack()
    warrior.numWeapon = 4
    println("The great warrior as now ${warrior.numWeapon} weapons")
}