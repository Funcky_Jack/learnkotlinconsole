fun main(args: Array<String>) {
    val str = "May the force be with you."
    println(str)

    val str1 = "My dad said the funniest thing he said \"A joke\" "
    println(str1)

    //Strings com multiplas linhas tem que estar compreendidas entre aspas triplas
    val rawCrawl = """|A long time ago,
        |in a galaxy
        |far, far, away...
        |BUMM BUMM BUMMMM""".trimMargin() //Remove as margens e o carater por defeito de prefixo é '|'
    println(rawCrawl)

    val rawCrawl1 = """*A long time ago,
        *in a galaxy
        *ar, far, away...
        *BUMM BUMM BUMMMM""".trimMargin("*") //Remoção das margens usando outro carater
    println(rawCrawl1)

    /*for (char in str) {
        println(char)
    }*/

    val contentEquals = str.contentEquals("Ma the force be with you.")
    println(contentEquals)

    val contains = str.contains("Force", false)
    println(contains)

    val uppercase = str.toUpperCase()
    val lower = str.toLowerCase()

    println(uppercase)
    println(lower)

    val num = 6
    val stringNum = num.toString()
    println(stringNum)

    val subsequence = str.subSequence(4, 13)
    println(subsequence)

    val luke = "Luke Skywalker"
    val lightSaberColor = "green"
    val vehicle = "landspeeder"
    val age = 27

    println("$luke has a $lightSaberColor lightsaber and drives a $vehicle and is $age years old")
    println("Luke full name $luke has ${luke.length} characters")


}