fun main(args: Array<String>) {

    val rebels = arrayListOf("Leiah", "Luke", "Han Solo", "Mon Mothma")

    val rebelVeihcles = hashMapOf("Solo" to "Millenium Falcon", "Luke" to "Landspeeder", "Boba Fett" to "Rocket Pack")

    for (rebel in rebels) {
        println("Name: $rebel")
    }

    for ((Key, Value) in rebelVeihcles) {
        println("$Key gets around in their $Value")
    }

    var x = 0

    while (x <= 10) {
        println(x)
        x++
    }



}