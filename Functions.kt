fun main(args: Array<String>) {

    fun forceChoke() {
        println("You have failed me for the last time Admiral...")
    }

    forceChoke()

    fun makeAnEntrance(line: String) {
        println(line)
    }

    makeAnEntrance("I find your lake of faith disturbing.")

    fun calculateNumberGoodGuys(rebels: Int, eowks: Int): Int {
        val goodGuys = rebels + eowks
        return goodGuys
    }

    val rebels = calculateNumberGoodGuys(5, 7)
    println("Darth Vader faced off against $rebels rebel scum")
    println("Darth Vader faced off against ${calculateNumberGoodGuys(5, 7)} rebel scum")

    fun vaderIsFeeling(mood: String = "angry") {
        println(mood)
    }

    vaderIsFeeling()
    vaderIsFeeling("meh")

    fun lukeShowsUp() {
        val lukeReturns = "Father be cool, take a cup of tea."
        println(lukeReturns)
    }

    lukeShowsUp()

    val lukeReturns = "Father why are you so angry, please take a cup of tea."

    fun lukeShowsUpFaceHim(talks: String) {
        println(talks)
    }

    lukeShowsUpFaceHim(lukeReturns)



}