fun main(args: Array<String>) {

    // Variável do tipo string que pode ser do tipo null
    var nullableName : String? = "Do I really exist?"
    nullableName = null

    // Null check
    var length = 0
    if (nullableName != null) {
        length = nullableName.length
    } else {
        length = -1
    }
    println(length)

    val l = if (nullableName != null) nullableName.length else -1
    println(l)

    // Second method Safe Call Operator ?
    println(nullableName?.length)

    //Third method is Elvis Operator
    val len = nullableName?.length ?: -1
    val noName = nullableName ?: "No one Knows me..."

    println(noName)

    // !! (Para quando temos a certeza que a nossa variável não é null
    //println(nullableName!!.length)


}