fun main(args: Array<String>) {

    /*Lists*/
    //Imutável - que não pode ser modificado
    val imperials = listOf("Emperor", "Darth Vader", "Boba Fett", "Tarkin")
    println(imperials.sorted())
    println(imperials[2])
    println(imperials.first())
    println(imperials.last())
    println(imperials.contains("Luke"))
    println(imperials)

    //Mutável - que pode ser modificado
    val rebels = arrayListOf("Leiah", "Luke", "Han Solo", "Mon Mothma")
    println(rebels.size)
    rebels.add("Chewbacca")
    println(rebels)
    rebels.add(0, "Lando")
    println(rebels)
    println(rebels.indexOf("Luke"))

    rebels.remove("Han Solo")
    println(rebels)

    /*Maps*/
    //Imutável - que não pode ser modificado
    //Chave e valor
    val rebelVeihclesMap = mapOf("Solo" to "Millenium Falcon", "Luke" to "Landspeeder")
    println(rebelVeihclesMap["Solo"])
    println(rebelVeihclesMap.get("Solo"))
    println(rebelVeihclesMap.getOrDefault("Leiah", "This ship does not exist."))
    println(rebelVeihclesMap.values)

    //Mutável - que pode ser modificado
    val rebelVeihcles = hashMapOf("Solo" to "Millenium Falcon", "Luke" to "Landspeeder", "Boba Fett" to "Rocket Pack")
    rebelVeihcles["Luke"] = "XWing"
    rebelVeihcles.put("Leiah", "Tantive IV")
    println(rebelVeihcles)
    rebelVeihcles.remove("Boba Fett")
    println(rebelVeihcles)

}